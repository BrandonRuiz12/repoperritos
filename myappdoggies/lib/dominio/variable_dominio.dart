class RazaFormada {
  late final String valor;

  RazaFormada._(this.valor);

  factory RazaFormada.constructor(String propuesta) {
    if (propuesta.trim().isEmpty) {
      throw ('La Raza esta mal formada');
    }
    return RazaFormada._(propuesta);
  }
}
