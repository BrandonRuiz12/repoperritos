import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:myappdoggies/caracteristicas/verificacion/bloc.dart';

void main() {
  /*
    Creamos un bloctest que verificará que el evento de creado emita el estado de SolicitandoRaza
    Para esto, en el build se utiliza el subjectBloc que en este caso es BlocVerificacion.
    En el act se manda el evento que emitirá el estado esperado, en este caso es el evento de Creado.
    Por último, en el expect, se espera que el evento emita un estado que sea SolicitandoRaza.
  */

  blocTest<BlocVerificacion, Estado>(
    'Deberá pasar si al llamar el evento de creado, se emite el estado de SolicitandoRaza',
    build: () => BlocVerificacion(),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<SolicitandoRaza>()],
  );
}
