class RegistroRaza {
  late final String estado;
  late final List<String> listaSubRazas;

  RegistroRaza._(this.estado, this.listaSubRazas);

  factory RegistroRaza.constructor({
    required String propuestaEstado,
    required List<String> propuestaListaSubRazas,
  }) {
    final resultado = RegistroRaza._(propuestaEstado, propuestaListaSubRazas);

    return resultado;
  }
}
