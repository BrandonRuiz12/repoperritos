import 'package:flutter/material.dart';

class VistaSolicitandoRaza extends StatelessWidget {
  const VistaSolicitandoRaza({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Texto(),
        const CampoTexto(),
        TextButton(onPressed: () {}, child: const Text('OK'))
      ],
    );
  }
}

class CampoTexto extends StatelessWidget {
  const CampoTexto({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 50, width: 300, child: TextField());
  }
}

class Texto extends StatelessWidget {
  const Texto({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Escribe la Raza de Perro a Buscar',
      style: TextStyle(fontWeight: FontWeight.bold),
    );
  }
}
