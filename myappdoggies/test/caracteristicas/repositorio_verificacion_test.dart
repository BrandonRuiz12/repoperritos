import 'package:flutter_test/flutter_test.dart';
import 'package:myappdoggies/caracteristicas/repositorio_verificacion.dart';
import 'package:myappdoggies/dominio/variable_dominio.dart';

void main() {
  group('Pruebas Offline', () {
    test('con mastiff me regresa valor', () {
      RepositorioPruebasVerificacion repositorio =
          RepositorioPruebasVerificacion();

      var resultado =
          repositorio.obtenerRegistroRaza(RazaFormada.constructor('mastiff'));

      resultado.match((l) {}, (r) {
        expect(r.listaSubRazas, equals(["bull" "english", "tibetan"]));
        expect(r.estado, equals("success"));
      });
    });
  });
}
