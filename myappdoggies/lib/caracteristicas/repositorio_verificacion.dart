import 'dart:convert';

import 'package:myappdoggies/dominio/registro_raza.dart';
import 'package:myappdoggies/dominio/variable_dominio.dart';

import '../dominio/problema.dart';
import 'package:fpdart/fpdart.dart';

abstract class RepositorioVerificacion {
  Either<Problema, RegistroRaza> obtenerRegistroRaza(RazaFormada raza);
}

List campos = ['message', 'status'];

class RepositorioPruebasVerificacion extends RepositorioVerificacion {
  final String _mastiff = """
  {"message":["bull","english","tibetan"],
  "status":"success"}
  """;
  final String _akita = """
  {"message":[],
  "status":"success"}
  """;
  final String _gato = """
  {"status":"error",
  "message":"Breed not found (master breed does not exist)",
  "code":404}
  """;

  @override
  Either<Problema, RegistroRaza> obtenerRegistroRaza(RazaFormada raza) {
    if (raza.valor == 'hound') {
      final documento = jsonDecode(_mastiff);
      return obtenerRazaDesdeJson(documento);
    }
    if (raza.valor == 'akita') {
      final documento = jsonDecode(_akita);
      return obtenerRazaDesdeJson(documento);
    }
    if (raza.valor == 'gato') {
      final documento = jsonDecode(_gato);
      return obtenerRazaDesdeJson(documento);
    }
    return Left(RazaNoRegistrada());
  }
}

Either<Problema, RegistroRaza> obtenerRazaDesdeJson(String documento) {
  Map<String, dynamic> resultado = jsonDecode(documento);

  if (resultado['message'].isEmpty || resultado['status'] == "error") {
    return left(RazaNoRegistrada());
  }

  return Right(RegistroRaza.constructor(
      propuestaListaSubRazas: resultado['message'],
      propuestaEstado: resultado['status']));
}
