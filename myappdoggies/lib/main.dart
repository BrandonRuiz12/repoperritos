import 'package:flutter/material.dart';
import 'package:myappdoggies/caracteristicas/verificacion/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './caracteristicas/vistas/vista_creandose.dart';
import './caracteristicas/vistas/vista_solicitando_raza.dart';

void main() {
  runApp(const AplicacionInyectada());
}

class AplicacionInyectada extends StatelessWidget {
  const AplicacionInyectada({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        BlocVerificacion blocVerificacion = BlocVerificacion();
        Future.delayed(const Duration(seconds: 2), () {
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const MiAplicacion(),
    );
  }
}

class MiAplicacion extends StatelessWidget {
  const MiAplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MiApp',
      home: Scaffold(
        body: Builder(builder: (context) {
          var estado = context.watch<BlocVerificacion>().state;
          if (estado is Creandose) {
            return const Center(
              child: VistaCreandose(),
            );
          }
          if (estado is SolicitandoRaza) {
            return const Center(child: VistaSolicitandoRaza());
          }
          return const Center(
              child: Text(
            'No deberias de ver esto',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 30, color: Colors.red),
          ));
        }),
      ),
    );
  }
}
