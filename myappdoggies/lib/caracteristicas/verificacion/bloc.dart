import 'package:bloc/bloc.dart';
import 'package:myappdoggies/dominio/variable_dominio.dart';

class Estado {}

class Creandose extends Estado {}

class SolicitandoRaza extends Estado {}

class Evento {}

class Creado extends Evento {}

class RazaRecibida extends Evento {
  final RazaFormada raza;
  RazaRecibida(this.raza);
}

//el bloc o (subjectbloc) se utiliza para determinar los cambios de estado mediante los eventos
/*
Creamos un subjectbloc que a su vez extiende la clase de Bloc, que recibe como parametros el primer evento y el primer
estado, en este caso es la clase de Evento y Estado.
en super, mandamos el primer estado generad, en este caso es el de Creandose.
Luego ponemos el evento que seguirá del estado de Creandose, que será lo que realice el cambio de estado.
Dentro del evento, emitiremos el siguiente estado, con un emit(estadosiguiente()), que contendrá el estado siguiente.
*/

class BlocVerificacion extends Bloc<Evento, Estado> {
  BlocVerificacion() : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(SolicitandoRaza());
    });
  }
}
